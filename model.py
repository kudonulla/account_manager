"""This module holds domain type of classes.

"""


class Account(object):
    """Account class holds information of an account such as name, address, phone number and account type.

    Attributes:
        name (str): The name of the account holder
        address (str): The address of the account holder
        phone_number (str): The phone number

    """

    def __init__(self, name="", address="", phone_number=""):
        """This __init__ method of Account class takes the name, address and phone number.

        Args:
            name (str): The name on the account.
            address (str): The address on the account.
            phone_number (str): The phone number on the account.
        """
        self._name = name
        self._address = address
        self._phone_number = phone_number

    def __str__(self):
        return f"Name: {self.name}, Address: {self.address}, Phone: {self.phone_number}"

    @property
    def name(self):
        """The getter for the name on the account."""
        return self._name

    @property
    def address(self):
        """The getter for the address on the account."""
        return self._address

    @property
    def phone_number(self):
        """The getter for the phone number on the account."""
        return self._phone_number

    def to_dict(self):
        """Returns the dictionary representation of the account.

        Returns:
            dict: The dictionary data of account
        """
        return {"name": self.name, "address": self.address, "phone_number": self.phone_number}
