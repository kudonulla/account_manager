import os
import subprocess
import unittest

from .. import main
from ..model import Account


MAIN_SCRIPT_PATH = main.__file__
TEST_DATA_FILE = "test/test.json"


class TestMain(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.account_manager = main.AccountManager(file_path=TEST_DATA_FILE)
        cls.account_manager.add(name="test", address="test", phone="test")

    @classmethod
    def tearDownClass(cls):
        os.remove(TEST_DATA_FILE)

    def test_list_all(self):
        output = TestMain.account_manager.list_all(format="json")
        self.assertIsNotNone(output)

        output = TestMain.account_manager.list_all(db_names=["personal"], format="json")
        self.assertIsNotNone(output)

        cmd = ["python", MAIN_SCRIPT_PATH, "list"]
        subprocess.run(cmd)

    def test_search(self):
        self.account_manager.search(name="test", format="plain")

    def test_add(self):
        TestMain.account_manager.add(name="teeeest")
        TestMain.account_manager.remove(name="teeeest")

    def test_remove(self):
        name = "test_to_remove"
        TestMain.account_manager.add(name=name)
        message = TestMain.account_manager.remove(name=name)
        self.assertEqual(message, f"Successfully remove the account with name={name}")

    def test_remove_error(self):
        name = "non_existent"
        message = TestMain.account_manager.remove(name=name)
        self.assertEqual(message, f"Unable to find the account with the name={name} in the db_name=None...")

    def test_format_output(self):
        account = Account("test", "test", "0000000")
        account2 = Account("test2", "test2", "1111111")
        accounts = [account, account2]

        for out_format in ["plain", "xml", "json", "html", None]:
            output = TestMain.account_manager.format_output(accounts, format=out_format)
            print(output)

    def test_no_args(self):
        cmd = ["python", MAIN_SCRIPT_PATH]
        with self.assertRaises(subprocess.CalledProcessError):
            subprocess.check_output(cmd)


if __name__ == "__main__":
    unittest.main()
