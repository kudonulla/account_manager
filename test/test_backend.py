import os
import unittest

from ..backend import BackendService
from ..model import Account


TEST_DATA_FILE = "test/test.json"


class TestBackend(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):
        os.remove(TEST_DATA_FILE)

    def setUp(self):
        self.backend = BackendService(file_path=TEST_DATA_FILE)

    def test_load_data(self):
        self.backend = BackendService(file_path=TEST_DATA_FILE)

        self.assertIsNotNone(self.backend.data)

    def test_add_account(self):
        account = Account("test_add", "test", "0000000")
        self.backend.add_account(account, "personal")

        account = Account("test_add2", "test_add2", "1111111")
        self.backend.add_account(account, "new_db")
        self.backend.save()

    def test_search(self):
        name = "test"
        result = self.backend.search(name=name)
        self.assertEqual(len(result), 2, "Didn't find expected number of accounts..")

        result = self.backend.search(name=name, db_name="personal")
        self.assertEqual(len(result), 1, "Didn't find expected number of accounts..")

        result = self.backend.search(address="test")
        self.assertIsNotNone(result)

        result = self.backend.search(phone="0000")
        self.assertIsNotNone(result)

        result = self.backend.search(name="Test", name_exact_match=True)
        self.assertEqual(len(result), 0)

    def test_search_error(self):
        with self.assertRaises(ValueError):
            self.backend.search(name="test", db_name="non_existent")

    def test_remove_account(self):
        account = Account("test", "test", "0000000")
        account2 = Account("test2", "test2", "0000000")
        self.backend.add_account(account, "personal")
        self.backend.add_account(account2, "work")

        removed = self.backend.remove_account(account)
        self.assertTrue(removed, "Failed to remove the account..")

        removed = self.backend.remove_account(account2, "personal")
        self.assertFalse(removed, "Removed the account in the other db set..")

        removed = self.backend.remove_account(account2, "work")
        self.assertTrue(removed, "Failed to remove the account..")

    def test_save(self):
        self.backend.save()


if __name__ == "__main__":
    unittest.main()
