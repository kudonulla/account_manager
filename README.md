# account_manager

## Overview
The Account Manager is a commandline tool to manage set(s) of data, with an API to directly access the core functionality.

## Usage
`main.py` script implements CLI interface. Please run `python main.py -h` for more info on the usage.

## Structure
```
account_manager
|  backend.py
|  main.py
|  model.py
|  README.md
+--data
|    db.json
+--test
     test_backend.py
     test_main.py
```

### backend.py
Provides the backend service using a json file as an emulation of database.

### main.py
Provides the CLI access to the Account Manager tool, and `AccountManager` class provides the core functionality as an API.

### model.py
Contains the `Account` class used as an account model throughout the project.
