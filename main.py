import argparse

from backend import BackendService
from model import Account


OUTPUT_FORMATS = ["plain", "xml", "json", "html"]


class AccountManager(object):

    def __init__(self, file_path=None):
        """This __init__ method of AccountManager class takes the file_path as an optional parameter."""
        if not file_path:
            self.backend_service = BackendService()
        else:
            self.backend_service = BackendService(file_path=file_path)

    def list_all(self, **kwargs):
        """List all the accounts information. It optionally takes `db_names`.

        Args:
            **kwargs: The expected kwards are; db_names, format

        Returns:
            str: The list of all accounts in the optionally specified db_name in the provided format.
        """
        db_names = kwargs["db_names"] if "db_names" in kwargs else None
        format = kwargs["format"] if "format" in kwargs else None

        accounts = []
        if db_names:
            for db_name in db_names:
                accounts.extend(self.backend_service.search(db_name=db_name))
        else:
            accounts = self.backend_service.search()

        output = self.format_output(accounts, format=format)

        return output

    def search(self, **kwargs):
        """Search for accounts by name, address and/or phone number in the optionally specified db_name.
        When more than one parameter are provided, it will be AND operation, so only the accounts that at least
        partially match all the provided values will be returned.

        Args:
            **kwargs: The expected kwards are: name, address, phone, db_name, format

        Returns:
            str: The list of accounts that meet the provided criteria in the optionally specified db_name.
                The result will be printed in the provided format: "plain", "xml", "json", "html".
        """
        name = kwargs["name"] if "name" in kwargs else None
        address = kwargs["address"] if "address" in kwargs else None
        phone = kwargs["phone"] if "phone" in kwargs else None
        db_name = kwargs["db_name"] if "db_name" in kwargs else None
        format = kwargs["format"] if "format" in kwargs else None

        accounts = self.backend_service.search(name=name, address=address, phone=phone, db_name=db_name)

        output = self.format_output(accounts, format=format)

        return output

    def add(self, **kwargs):
        """Add an account with the provided name, address and/or phone in the optionally provided db_name data set.

        Args:
            **kwargs: The expected kwards are: name, address, phone, db_name

        Returns:
            str: The result message.
        """
        name = kwargs["name"]
        address = kwargs["address"] if "address" in kwargs else None
        phone = kwargs["phone"] if "phone" in kwargs else None
        db_name = kwargs["db_name"] if "db_name" in kwargs else "personal"

        account = Account(name, address, phone)

        # add the account and save the change
        self.backend_service.add_account(account, db_name)
        self.backend_service.save()

        output = f"Successfully added the following account: {account}"

        return output

    def remove(self, **kwargs):
        """Remove the account with the provided name. The name must be the exact match for the account to be removed.

        Args:
            **kwargs: The expected kwargs are: name, db_name

        Returns:
            str: The result message.
        """
        name = kwargs["name"]
        db_name = kwargs["db_name"] if "db_name" in kwargs else None

        accounts = self.backend_service.search(name=name, db_name=db_name, name_exact_match=True)

        if not accounts:
            output = f"Unable to find the account with the name={name} in the db_name={db_name}..."
        else:
            # TODO probably it's better to check why there are duplicate accounts with the same name
            # but for now, just removing them all
            for account in accounts:
                self.backend_service.remove_account(account)

            self.backend_service.save()

            output = f"Successfully remove the account with name={name}"

        return output

    @staticmethod
    def format_output(accounts, format=None):
        """Formats the list of accounts in the specified type of format: plain, xml, json, html

        Args:
            accounts (list): The list of Account objects to format.
            format (str): The format the output data to be in. Currently supported formats are: plain, xml, json, html

        Returns:
            str/list: The presentation of accounts in the specified format.
        """
        if format == "json":
            out = []
            for account in accounts:
                out.append(account.to_dict())
        elif format == "xml":
            out = "<accounts>"
            for account in accounts:
                out += "<account>"
                for key, value in account.to_dict().items():
                    out += f"<{key}>{value}</{key}>"
                out += "</account>"
            out += "</accounts>"
        elif format == "html":
            out = "<!DOCTYPE html><head><title>Accounts</title></head><body>"
            out += "<table>"
            if accounts:
                first = accounts[0]
                out += "<tr>"

            for key in first.to_dict().keys():
                out += f"<th>{key}</th>"
            out += "</tr>"

            for account in accounts:
                out += "<tr>"
                for value in account.to_dict().values():
                    out += f"<td>{value}</td>"
                out += "</tr>"
            out += "</table>"
            out += "</body></html>"
        else:
            out = ""
            for account in accounts:
                out += str(account)
                out += "\n"

        return out


if __name__ == "__main__":
    account_manager = AccountManager()

    parser = argparse.ArgumentParser(description="Awesome Account Manager")
    subparser = parser.add_subparsers(help="These four types of operations are supported.")

    list_parser = subparser.add_parser("list")
    list_parser.set_defaults(func=account_manager.list_all)
    list_parser.add_argument(
        "db_names",
        nargs="*",
        help="The names of data set to list accounts for. "
             "If not provided, it will list all accounts from all data sets.",
    )
    list_parser.add_argument(
        "-f", "--format",
        choices=OUTPUT_FORMATS,
        help="The format of the output data",
    )

    search_parser = subparser.add_parser("search")
    search_parser.add_argument(
        "-n", "--name",
        help="The name of the account holder to search by",
    )
    search_parser.add_argument(
        "-a", "--address",
        help="The address of the account holder to search by",
    )
    search_parser.add_argument(
        "-p", "--phone",
        help="The phone number of the account holder to by",
    )
    search_parser.add_argument(
        "-t", "--db_name",
        help="The type of db to search",
    )
    search_parser.add_argument(
        "-f", "--format",
        choices=OUTPUT_FORMATS,
        help="The format of the output data",
    )
    search_parser.set_defaults(func=account_manager.search)

    add_parser = subparser.add_parser("add")
    add_parser.add_argument(
        "name",
        help="The name of the account holder to add.",
    )
    add_parser.add_argument(
        "address",
        help="The address of the account holder to add.",
    )
    add_parser.add_argument(
        "phone",
        help="The phone number of the account holder to add.",
    )
    add_parser.add_argument(
        "--db_name",
        default="personal",
        help="The db name to add the account to. Will be added to \"personal\" if not provided.",
    )
    add_parser.set_defaults(func=account_manager.add)

    remove_parser = subparser.add_parser("remove")
    remove_parser.add_argument(
        "name",
        help="The name of the account to remove.",
    )
    remove_parser.add_argument(
        "--db_name",
        help="The db name to remove the account from. Will be removed from any DB if not provided.",
    )
    remove_parser.set_defaults(func=account_manager.remove)

    # parse the arguments and call the proper function
    args = parser.parse_args()
    kwargs = vars(args)
    if not kwargs:
        parser.error("No arguments provided..")

    output = kwargs["func"](**kwargs)

    print(output)
