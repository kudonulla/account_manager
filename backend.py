"""This module provides the backend service using a json file as an emulation of database.

"""
import json
import os

from model import Account


DEFAULT_DATA_FILE_PATH = "data/db.json"


class BackendService(object):

    def __init__(self, file_path=DEFAULT_DATA_FILE_PATH):
        """This __init__ method of BackendService class takes the file_path as an optional parameter.

        Args:
            file_path (str): The path to the backend json file. When not provided, the default data file path will be used.
        """
        self._data_file_path = file_path
        self._data = self.load_data(file_path)

    @property
    def data(self):
        """dict: The getter for the data dictionary that holds all the account information."""
        return self._data

    def load_data(self, file_path):
        """Load the account data from the json file with the provided path.

        Returns:
            dict: The retrieved data in the format:
                {
                    "personal": [Account object, ....],
                    "work": [Account object, ....],
                    ...
                }
        """
        data = {}
        if os.path.exists(file_path):
            with open(file_path, "r") as f:
                raw_data = json.load(f)

            for k, v in raw_data.items():
                data[k] = [Account(**x) for x in v]

        return data

    def save(self):
        """Save all the accounts information held in the memory to the data file.

        """
        raw_data = {}
        for k, v in self._data.items():
            raw_data[k] = [x.to_dict() for x in v]

        with open(self._data_file_path, "w") as f:
            json.dump(raw_data, f)

    def add_account(self, account, db_name):
        """Add a new account to the data set with the specified db name.

        Args:
            account (Account): An account object to add
            db_name (str): The name of the data set to add the account to.
        """
        # TODO validation?

        if db_name in self._data:
            self._data[db_name].append(account)
        else:
            self._data[db_name] = [account]

    def remove_account(self, account, db_name=None):
        """Remove an account from the data set with optionally specified db name.

        Args:
            account (Account): The account to remove.
            db_name (str): The db name to remove the account from.

        Returns:
            bool: True if it was successfully remove; False otherwise.
        """
        removed = False
        if db_name:
            if account in self._data[db_name]:
                self._data[db_name].remove(account)
                removed = True
        else:
            for k, v in self._data.items():
                for curr_account in v:
                    if account == curr_account:
                        self._data[k].remove(curr_account)
                        removed = True
                        break

        return removed

    def search(self, name=None, address=None, phone=None, db_name=None, name_exact_match=False):
        """Search for account(s) with the provided name, address and/or phone in the optionally provided db name.
            When db name not provided, it will search in all the data sets.

        Args:
            name (str): The (partial/whole) name on the account to search by.
            address (str): The (partial) address on the account to search by.
            phone (str): The (partial) phone number on the account to search by.
            db_name (str): The db name to search the account in.
            name_exact_match (bool): Set True to find the account with the exactly matching name;
                False to include the partial match.

        Returns:
            list: The list of accounts that match the criteria.
        """
        if db_name:
            if db_name not in self.data:
                raise ValueError("bd_name={} doesn't exist in the system.".format(db_name))

            # search only in the provided db_name account list
            all_accounts = self.data[db_name]
        else:
            # search in all the accounts across all db
            all_accounts = []
            for k, v in self.data.items():
                all_accounts.extend(v)

        result = all_accounts

        # filter the accounts by provided parameter(s)
        if name:
            if name_exact_match:
                result = [x for x in result if name == x.name]
            else:
                result = [x for x in result if name.lower() in x.name.lower()]
        if address:
            result = [x for x in result if address.lower() in x.address.lower()]
        if phone:
            result = [x for x in result if phone.lower() in x.phone_number.lower()]

        return result
